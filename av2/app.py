from config import *
from model import *


@app.route("/")
def home():
    return render_template("home.html")


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/listar_musicas")
def listar_musicas():
    with db_session:
        # obtém as pessoas
        musicas = Musica.select()
        return render_template("listar_musicas.html", musicas=musicas)


@app.route("/form_adicionar_musica")
def form_adicionar_musica():
    return render_template("form_adicionar_musica.html")


@app.route("/adicionar_musica")
def adicionar_musica():
    # obter os parâmetros
    musica = request.args.get("musica")
    cantor = request.args.get("cantor")
    genero = request.args.get("genero")
    # salvar
    with db_session:
        # criar a pessoa
        p = Musica(**request.args)
        # salvar
        commit()
        # encaminhar de volta para a listagem
        return redirect("listar_musicas")


'''
run:
$ flask run
'''

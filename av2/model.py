from config import *


class Musica(db.Entity):
    musica = Required(str)
    cantor = Required(str)
    genero = Optional(str)

    def __str__(self):
        return f'{self.musica}, {self.cantor}, {self.genero}'


db.bind(provider='sqlite', filename='person.db', create_db=True)
db.generate_mapping(create_tables=True)
